const { Router } = require('express')
const FighterService = require('../services/fighterService')
const { responseMiddleware } = require('../middlewares/response.middleware')
const { createFighterValid, updateFighterValid } = require(
  '../middlewares/fighter.validation.middleware')

const router = Router()

router.get('/', function (req, res, next) {
  const fighters = FighterService.getFighters()
  req.body = {
    done: {
      status: 200,
      body: fighters,
      canBeEmpty: true
    }
  }
  next()
}, responseMiddleware)

router.get('/:id', function (req, res, next) {
  const fighter = FighterService.getFighter({ id: req.params.id })
  req.body = {
    done: {
      status: 200,
      body: fighter,
      canBeEmpty: false
    },
    error: {
      status: 404,
      message: 'Fighter not found'
    }
  }
  next()
}, responseMiddleware)

router.post('/', createFighterValid, function (req, res, next) {
  const fighter = FighterService.createFighter(req.body)
  req.body = {
    done: {
      status: 200,
      body: fighter,
      canBeEmpty: false
    },
    error: {
      status: 400,
    }
  }
  next()
}, responseMiddleware)

router.put('/:id', updateFighterValid, function (req, res, next) {
  let fighter = null
  if (FighterService.getFighter({ id: req.params.id })) {
    fighter = FighterService.updateFighter(req.params.id, req.body)
  }
  req.body = {
    done: {
      status: 200,
      body: fighter,
      canBeEmpty: false
    },
    error: {
      status: 404,
      message: `Fighter not found`
    }
  }
  next()
}, responseMiddleware)

router.delete('/:id', function (req, res, next) {
  const fighter = FighterService.deleteFighter(req.params.id)[0]
  req.body = {
    done: {
      status: 200,
      body: fighter,
      canBeEmpty: false
    },
    error: {
      status: 404,
      message: `Fighter not found`
    }
  }
  next()
}, responseMiddleware)
module.exports = router

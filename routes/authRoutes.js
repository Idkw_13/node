const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
  console.log(req.body);
    try {
      if(req.body.email && req.body.password){
        const user = AuthService.login({email: req.body.email, password: req.body.password})
        console.log(user, 'user')
        req.body = {
          done: {
            body: user ,
            status: 200
          },
          error: {
            status: 403,
            message: 'Invalid login credentials'
          }
        }
      }

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;

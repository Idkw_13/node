const { Router } = require('express')
const UserService = require('../services/userService')
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware')
const { responseMiddleware } = require('../middlewares/response.middleware')
const router = Router()

router.get('/', function (req, res, next) {
  const users = UserService.getUsers()
  req.body = {
    done: {
      status: 200,
      body: users,
      canBeEmpty: true
    }
  }
  next()
}, responseMiddleware)

router.get('/:id', function (req, res, next) {
  const user = UserService.getUser({ id: req.params.id })
  req.body = {
    done: {
      status: 200,
      body: user,
      canBeEmpty: false
    },
    error: {
      status: 404,
      message: 'User not found'
    }
  }
  next()
}, responseMiddleware)

router.post('/', createUserValid, function (req, res, next) {
  const user = UserService.createUser(req.body)
  req.body = {
    done: {
      status: 200,
      body: user,
      canBeEmpty: false
    },
    error: {
      status: 400,
    }
  }
  next()
}, responseMiddleware)

router.put('/:id', updateUserValid, function (req, res, next) {
  let user = null;
  if(UserService.getUser({ id: req.params.id })){
     user = UserService.updateUser(req.params.id, req.body)
  }
  req.body = {
    done: {
      status: 200,
      body: user,
      canBeEmpty: false
    },
    error: {
      status: 404,
      message: `User not found`
    }
  }
  next()
}, responseMiddleware)

router.delete('/:id', function (req, res, next) {
  const user = UserService.deleteUser(req.params.id)[0]
  req.body = {
    done: {
      status: 200,
      body: user,
      canBeEmpty: false
    },
    error: {
      status: 404,
      message: `User not found`
    }
  }
  next()
}, responseMiddleware)
module.exports = router

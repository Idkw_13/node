const { UserRepository } = require('../repositories/userRepository')

class UserService {
  getUsers () {
    return UserRepository.getAll()
  }

  getUser (search) {
    return UserRepository.getOne(search)
  }

  createUser (data) {
    return UserRepository.create(data);
  }
  updateUser(id, user){
    return UserRepository.update(id, user)
  }
  deleteUser(id){
    return UserRepository.delete(id)
  }
  search (search) {
    const item = UserRepository.getOne(search)
    if (!item) {
      return null
    }
    return item
  }
}

module.exports = new UserService()

const { FighterRepository } = require('../repositories/fighterRepository')

class FighterService {
  getFighters () {
    return FighterRepository.getAll()
  }

  getFighter (search) {
    return FighterRepository.getOne(search)
  }

  createFighter (data) {
    return FighterRepository.create(data)
  }

  updateFighter (id, user) {
    return FighterRepository.update(id, user)
  }

  deleteFighter (id) {
    return FighterRepository.delete(id)
  }
}

module.exports = new FighterService()

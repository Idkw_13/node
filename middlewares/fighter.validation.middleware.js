const { fighter } = require('../models/fighter')
const validateFighter = (req, res, checkId, skip) => {
  const body = req.body
  const fighterData = {}
  for (let key in body) {
    if (body.hasOwnProperty(key)) {
      if (fighter.hasOwnProperty(key)) {
        fighterData[key] = body[key]
      }
    }
  }
  if (fighterData['id']) {
    res.status(400).send({
      error: true,
      message: 'Fighter is not valid'
    })
  }
  let addictionSkip = 1
  if (skip) {
    addictionSkip = 0
  }
  if (Object.keys(fighter).length - addictionSkip !== Object.keys(fighterData).length) {
    res.status(400).send({
      error: true,
      message: 'Fighter is not valid'
    })
  }
  for (let key in fighterData) {
    if (body.hasOwnProperty(key)) {
      if (!body[key]) {
        return res.status(400).send({
          error: true,
          message: `${key} is not valid`
        })
      }
      if (checkId && body[key] === 'id') {
        delete body[key]
      }
      if (key === 'defense') {
        if (body[key] > 10) {
          return res.status(400).send({
            error: true,
            message: 'Defense is not valid'
          })
        }
      }
    }

  }
  req.body = fighterData
}
const createFighterValid = (req, res, next) => {
  if (!validateFighter(req, res,)) {
    next()
  }
}

const updateFighterValid = (req, res, next) => {
  if (!validateFighter(req, res, false, true)) {
    next()
  }
}

exports.createFighterValid = createFighterValid
exports.updateFighterValid = updateFighterValid

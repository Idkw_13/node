const responseMiddleware = (req, res, next) => {
  if (req.body) {
    if (req.body.done && req.body.done.body || req.body.canBeEmpty) {
      res.status(req.body.done.status).json(req.body.done.body)
    } else {
      res.status(req.body.error.status).send({
        error: true,
        message: req.body.error.message
      })
    }
  }

  next()
}

exports.responseMiddleware = responseMiddleware

const { user } = require('../models/user')
const validateUser = (req, res, checkId, skip) => {
  const body = req.body
  const userData = {}
  for (let key in body) {
    if (body.hasOwnProperty(key)) {
      if (user.hasOwnProperty(key)) {
        userData[key] = body[key]
      }
    }
  }
  if (userData['id']) {
    res.status(400).send({
      error: true,
      message: 'User is not valid'
    })
  }
  let addictionSkip = 1;
  if(skip){
    addictionSkip = 0
  }
  if (Object.keys(user).length - addictionSkip !== Object.keys(userData).length) {
    res.status(400).send({
      error: true,
      message: 'User is not valid'
    })
  }

  for (let key in userData) {
    if (body.hasOwnProperty(key)) {
      if (!body[key]) {
        return res.status(400).send({
          error: true,
          message: `${key} is not valid`
        })
      }
      if (checkId && body[key] === 'id') {
        delete body[key]
      }
      if (key === 'email') {
        if (!body[key].endsWith('@gmail.com')) {
          return res.status(400).send({
            error: true,
            message: 'Email is not valid'
          })
        }
      }
      if (key === 'phoneNumber') {
        if (!body[key].startsWith('+380') || String(body[key]).length !== 13) {
          return res.status(400).send({
            error: true,
            message: 'Phone is not valid'
          })
        }
      }
      if (key === 'password') {
        if (body[key].length < 3) {
          return res.status(400).send({
            error: true,
            message: 'Password is not valid'
          })
        }
      }

    }

  }
  req.body = userData
}
const createUserValid = (req, res, next) => {
  if (!validateUser(req, res,)) {
    next()
  }
}
const updateUserValid = (req, res, next) => {
  if (!validateUser(req, res, false, true)) {
    next()
  }
}

exports.createUserValid = createUserValid
exports.updateUserValid = updateUserValid
